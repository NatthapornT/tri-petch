const CarouselFootballer = () => {
    return (
        <div className="container-carousel">
            <div id="myCarousel" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li
                        data-target="#myCarousel"
                        data-slide-to="0"
                        className="active"
                    ></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <div className="carousel-inner">
                    <div className="item active">
                        <div className="item-no">01</div>
                        <div className="line-num"></div>
                        <div className="head-item">CONNECTION</div>
                        <div className="detail">
                            Connect with coaches directly, you can ping coaches to view
                            profile.
                        </div>
                    </div>

                    <div className="item">
                        <div className="item-no">02</div>
                        <div className="line-num"></div>
                        <div className="head-item">COLLABORATION</div>

                        <div className="detail">
                            Work with other student athletes to increase visability. When
                            you share and like other players videos it will increase your
                            visability as a player. This is the team work aspect to
                            Surface 1.
                        </div>
                    </div>

                    <div className="item">
                        <div className="item-no">03</div>
                        <div className="line-num"></div>
                        <div className="head-item">GROWTH</div>
                        <div className="detail">
                            Resources and tools for you to get better as a student
                            Athelte. Access to training classes, tutor sessions, etc
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CarouselFootballer;