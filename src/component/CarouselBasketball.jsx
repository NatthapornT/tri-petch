const CarouselBasketball = () => {
    return (
        <div className="container-carousel">
            <div
                id="myCarousel-basketball"
                className="carousel slide"
                data-ride="carousel"
            >
                <ol className="carousel-indicators">
                    <li
                        data-target="#myCarousel-basketball"
                        data-slide-to="0"
                        className="active"
                    ></li>
                    <li data-target="#myCarousel-basketball" data-slide-to="1"></li>
                    <li data-target="#myCarousel-basketball" data-slide-to="2"></li>
                </ol>

                <div className="carousel-inner">
                    <div className="item active">
                        <div className="item-no">01</div>
                        <div className="line-num"></div>
                        <div className="head-item">CONNECTION</div>
                        <div className="detail">
                            Connect with talented athlete directly, you can watch their
                            skills through video showreels directly from Surface 1.
                        </div>
                    </div>

                    <div className="item">
                        
                        <div className="item-no">02</div>
                        <div className="line-num"></div>
                        <div className="head-item">COLLABORATION</div>

                        <div className="detail">
                            Work with recruiter to increase your chances of finding
                            talented athlete.
                        </div>
                    </div>

                    <div className="item">
                        <div className="item-no">03</div>
                        <div className="line-num"></div>
                        <div className="head-item">GROWTH</div>
                        <div className="detail">
                            Save your time, recruit proper athlets for your team.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CarouselBasketball;