import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "./styled.css";
import Path from "./assets/images/path.svg"
import Plus from "./assets/images/plus.svg"
import CarouselFootballer from "./component/CarouselFootballer";
import CarouselBasketball from "./component/CarouselBasketball";
const SportWrapper = () => {
    return (
        <div className="container">
            <div className="content w-100">
                <div className="content-footballer-xl">
                    <div className="section-footballer">
                        <img
                            src={require("./assets/images/footballer.png")}
                            alt=""
                            className="img-football"
                        />
                        <img
                            src={Path}
                            alt=""
                            className="img-path football"
                        />
                        <img
                            src={Path}
                            alt=""
                            className="img-path-lg football"
                        />
                        <img
                            src={Plus}
                            alt=""
                            className="img-plus1 football"
                        />
                        <img
                            src={Plus}
                            alt=""
                            className="img-plus3 football"
                        />
                        <div className="text-athlets-mb">ATHLETS</div>
                        <div className="row-head">
                            <div className="offset-6 col-6 text-athlets">ATHLETS</div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="section-connection offset-6 col-6">
                            <div className="item-no1 ">01</div>
                            <div className="line-num1 "></div>
                            <div className="head-item1 ">CONNECTION</div>
                            <div className="detail1">
                                Connect with coaches directly, you can ping coaches to view
                                profile.
                            </div>
                        </div>
                    </div>
                    <div className="row section-collab ">
                        <div className="section-collab offset-6 col-6">
                            <div className="item-no">02</div>
                            <div className="line-num"></div>
                            <div className="head-item">COLLABORATION</div>

                            <div className="detail">
                                Work with other student athletes to increase visability. When
                                you share and like other players videos it will increase your
                                visability as a player. This is the team work aspect to
                                Surface 1.
                            </div>
                        </div>
                    </div>
                    <div className="row section-growth">
                        <div className="section-growth offset-6 col-6">
                            <div className="item-no">03</div>
                            <div className="line-num bg-white"></div>
                            <div className="head-item">GROWTH</div>
                            <div className="detail text-white">
                                Resources and tools for you to get better as a student
                                Athelte. Access to training classes, tutor sessions, etc
                            </div>
                        </div>
                    </div>


                    <CarouselFootballer />
                </div>





                <div className="content-basketball">
                    <div className="section-basketball">
                        <img
                            src={require("./assets/images/basketball.png")}
                            alt=""
                            className="img-basketball"
                        />
                        <img
                            src={Path}
                            alt=""
                            className="img-path"
                        />
                        <img
                            src={Path}
                            alt=""
                            className="img-path-lg"
                        />
                        <img
                            src={Plus}
                            alt=""
                            className="img-plus1"
                        />
                        <img
                            src={Plus}
                            alt=""
                            className="img-plus2"
                        />
                        <img
                            src={Plus}
                            alt=""
                            className="img-plus3"
                        />
                        <div className="text-athlets-mb">PLAYERS</div>
                        <div className="row">
                            <div className="text-player offset-2 col-4">PLAYERS</div>
                        </div>
                    </div>

                    <div className="row section-connection">
                        <div className="section-connection basketboll offset-2 col-4">
                            <div className="item-no1 item-no1-bas">01</div>
                            <div className="line-num1 line-num1-bas"></div>
                            <div className="head-item1 head-item1-bas">CONNECTION</div>
                            <div className="detail1 detail1-bas">
                                Connect with talented athlete directly, you can watch their
                                skills through video showreels directly from Surface 1.
                            </div>
                        </div>
                    </div>
                    <div className="row section-collab ">
                        <div className="section-collab offset-2 col-4">
                            <div className="item-no">02</div>
                            <div className="line-num"></div>
                            <div className="head-item">COLLABORATION</div>

                            <div className="detail">
                                Work with recruiter to increase your chances of finding
                                talented athlete.
                            </div>
                        </div>
                    </div>
                    <div className="row section-growth-bas">
                        <div className="section-growth-bas offset-2 col-4">
                            <div className="item-no bas">03</div>
                            <div className="line-num bg-white"></div>
                            <div className="head-item">GROWTH</div>
                            <div className="detail text-white">
                                Save your time, recruit proper athlets for your team.
                            </div>
                        </div>
                    </div>

                    <CarouselBasketball />
                </div>
            </div>
        </div>
    );
};

export default SportWrapper;
